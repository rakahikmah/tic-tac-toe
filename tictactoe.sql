-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Mar 2019 pada 17.29
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tictactoe`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `activities`
--

CREATE TABLE `activities` (
  `id_activities` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `id_room` int(11) NOT NULL,
  `time_activities` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activities`
--

INSERT INTO `activities` (`id_activities`, `action`, `id_room`, `time_activities`) VALUES
(8, 'PLAYER 1 run X', 3, '2019-03-09 15:20:51'),
(9, 'PLAYER 2 run 0', 3, '2019-03-09 15:20:52'),
(10, 'PLAYER 1 run X', 3, '2019-03-09 15:20:54'),
(11, 'PLAYER 2 run 0', 3, '2019-03-09 15:20:56'),
(12, 'PLAYER 1 run X', 3, '2019-03-09 15:20:57'),
(13, 'PLAYER 2 run 0', 3, '2019-03-09 15:20:59'),
(14, 'PLAYER 1 run X', 4, '2019-03-09 15:48:19'),
(15, 'PLAYER 2 run 0', 4, '2019-03-09 15:48:19'),
(16, 'PLAYER 1 run X', 4, '2019-03-09 15:48:20'),
(17, 'PLAYER 2 run 0', 4, '2019-03-09 15:48:20'),
(18, 'PLAYER 1 run X', 4, '2019-03-09 15:48:21'),
(19, 'PLAYER 2 run 0', 4, '2019-03-09 15:48:22'),
(20, 'PLAYER 1 run X', 4, '2019-03-09 15:48:24'),
(21, 'PLAYER 2 run 0', 4, '2019-03-09 15:48:25'),
(22, 'PLAYER 1 run X', 4, '2019-03-09 15:48:27'),
(23, 'PLAYER 1 run X', 4, '2019-03-09 15:53:32'),
(24, 'PLAYER 2 run 0', 4, '2019-03-09 15:53:32'),
(25, 'PLAYER 1 run X', 4, '2019-03-09 15:53:33'),
(26, 'PLAYER 2 run 0', 4, '2019-03-09 15:53:35'),
(27, 'PLAYER 1 run X', 4, '2019-03-09 15:53:37'),
(28, 'PLAYER 2 run 0', 4, '2019-03-09 15:53:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `name_room` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rooms`
--

INSERT INTO `rooms` (`id_room`, `status`, `name_room`) VALUES
(3, 1, 'game 1'),
(4, 2, 'game 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `scores`
--

CREATE TABLE `scores` (
  `id_scores` int(11) NOT NULL,
  `against` varchar(10) NOT NULL,
  `winner` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_rooms` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `scores`
--

INSERT INTO `scores` (`id_scores`, `against`, `winner`, `created_at`, `id_rooms`) VALUES
(1, 'friend', '(0) Player 2', '2019-03-09 21:20:59', 3),
(2, 'friend', '(X) Player 1', '2019-03-09 21:48:27', 4);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id_activities`),
  ADD KEY `id_room_idx` (`id_room`);

--
-- Indeks untuk tabel `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`);

--
-- Indeks untuk tabel `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id_scores`),
  ADD KEY `fk_scores_idx` (`id_rooms`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `activities`
--
ALTER TABLE `activities`
  MODIFY `id_activities` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `scores`
--
ALTER TABLE `scores`
  MODIFY `id_scores` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `id_room` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id_room`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `scores`
--
ALTER TABLE `scores`
  ADD CONSTRAINT `fk_scores` FOREIGN KEY (`id_rooms`) REFERENCES `rooms` (`id_room`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
