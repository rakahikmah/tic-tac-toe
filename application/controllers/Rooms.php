<?php
class Rooms extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('rooms_model');
                $this->load->helper(array('url_helper','form','url'));
                $this->load->library('form_validation');
                date_default_timezone_set("Asia/Jakarta");
        }

        public function create()
        {         
            $this->form_validation->set_rules('nameroom', 'nameroom', 'required');
            
            if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('rooms/createroom');
                }
                else
                {
                        $name = $this->input->post('nameroom');

                        $data = array(
                            'name_room'=>$name,
                            'status'=>0
                        );

                        $this->rooms_model->create($data);
                        redirect('welcome');
                }
        }

        public function startgame()
        {
            $idroom = $this->input->post('room');

            $data = $this->rooms_model->updatestart($idroom);

            echo json_encode($data);
        }

        public function finishgame($id)
        {
            $this->rooms_model->finishgame($id);
        }
}
