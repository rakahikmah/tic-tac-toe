<?php
class Tictactoe extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('scores_model');
                $this->load->helper(array('url_helper','form','url'));
                $this->load->library('form_validation');
                date_default_timezone_set("Asia/Jakarta");
        }


        public function index()
        {                
            
                $data['scores'] = $this->scores_model->get_scores();
                $data['title'] = 'TicTacToe';
                $data['room']=$this->uri->segment(3);
                $this->load->view('templates/header', $data);
                $this->load->view('tictactoe/index', $data);
                $this->load->view('templates/footer');
        }

}
