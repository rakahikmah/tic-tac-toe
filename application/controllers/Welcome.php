<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


    public function __construct()
    {
                parent::__construct();
				$this->load->helper('url_helper');
				$this->load->model('rooms_model');
                $this->load->helper(array('url_helper','form','url'));
				$this->load->library('form_validation');
				date_default_timezone_set("Asia/Jakarta");
    }
    
	public function index()
	{
		$data['rooms'] = $this->rooms_model->showroom();
		$this->load->view('welcome_message',$data);
	}
}
