<?php
class Activities extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('activities_model');
                $this->load->helper(array('url_helper','form','url'));
                $this->load->library('form_validation');
        }

    
        public function create()
        {
            $nilai = $this->input->post('valuet');
            $nameplayer = $this->input->post('nameplayer');
            $room = $this->input->post('room');

            
            $data = array (
                    'action'=>$nameplayer." run ".$nilai ,
                    'id_room'=>$room,
                    'time_activities'=>date('Y-m-d H:i:s')
            );

            $hasil = $this->activities_model->create($data);

            echo json_encode($hasil);


        }

}
