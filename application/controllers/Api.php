<?php
class Api extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('url_helper','form','url'));
                $this->load->library('form_validation');
                $this->load->database();
        }

        // api untuk create room
        public function createroom()
        {
            $this->db->select("*");
            $this->db->from('rooms');
            $query = $this->db->get();

            $result = $query->result_array();

            $dataarray = array();
            $resultarray = array();

            foreach ($result as $value) {

                $dataarray = array(
                    'id_rooms'=>$value['id_room'],
                    'name_room'=>$value['name_room'],
                    'link'=>base_url()."tictactoe"."/index"."/".$value['id_room']
                );

                array_push($resultarray,$dataarray);
            }

            echo json_encode($resultarray);
        }


        // api untuk create room
        public function enterroom()
        {
            $this->db->select("*");
            $this->db->from('rooms');
            $query = $this->db->get();

            $result = $query->result_array();

            $dataarray = array();
            $resultarray = array();

            foreach ($result as $value) {

                if ($value['status'] == 0) {
                    $status = "Lest start game";
                } else if($value['status'] == 1){
                    $status = "Playing game";
                }else if($value['status']==2){
                    $status = "Finish Game";
                }
                

                $dataarray = array(
                    'id_rooms'=>$value['id_room'],
                    'name_room'=>$value['name_room'],
                    'status'=>$status
                );

                array_push($resultarray,$dataarray);
            }

            echo json_encode($resultarray);
        }

        public function playermove()
        {
            $this->db->select("*");
            $this->db->from('activities');
            $this->db->join('rooms','rooms.id_room=activities.id_room');
            
            $query = $this->db->get();

            $result = $query->result_array();

            $id_room=$result[0];   

            $newkey=0;
          
            $resultactivities[$id_room]="";
          
            foreach ($result as $key => $val) {
             if ($id_room==$val['id_room']){
               $resultactivities[$id_room][$newkey]=$val;
             } else {
               $resultactivities[$val['id_room']][$newkey]=$val;
             }
               $newkey++;       
            }
            
          echo json_encode($resultactivities);
            
        }

        public function evaluated()
        {
            $this->db->select("*");
            $this->db->from('scores');
            $this->db->join('rooms','rooms.id_room=scores.id_rooms');

            $query = $this->db->get();

            $result = $query->result_array();

            echo json_encode($result);


           
        }



}
