<?php
class Rooms_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
    
        public function create($param)
        {
            $this->db->insert('rooms', $param);
            
        }

        public function showroom()
        {
            $this->db->order_by("id_room", "DESC");
            $query = $this->db->get('rooms');
            return $query->result_array();
        }

        public function updatestart($id)
        {
            $data = array(
                'status' => 1,
            );
        
            $this->db->where('id_room', $id);
            return $this->db->update('rooms', $data);
        }

        public function finishgame($id)
        {
            $data = array(
                'status' => 2
            );

            $this->db->where('id_room',$id);
            return $this->db->update('rooms',$data);
        }

}
