<?php
class Scores_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
    
        public function get_scores($slug = FALSE)
        {
                if ($slug === FALSE)
                {
                      
                        $this->db->order_by("id_scores", "DESC");
                        $query = $this->db->get('scores');
                        return $query->result_array();
                }

                $query = $this->db->get_where('scores', array('slug' => $slug));
                return $query->row_array();
        }
    
        public function set_scores()
        {
            $this->load->helper('url');

            $data = array(
                'against' => $this->input->post('against'),                
                'winner' => $this->input->post('winner'),
                'id_rooms'=> $this->input->post('room')
            );
            log_message('info', 'new game scores added in db > data: '.print_r($data, true));
            
            $data2 = array(
                'status' => 2
            );

            $this->db->where('id_room',$this->input->post('room'));
            $this->db->update('rooms',$data2);

            return $this->db->insert('scores', $data);
        }
}
